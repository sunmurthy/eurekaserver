FROM openjdk:8
ADD target/eureka-0.0.1-SNAPSHOT.jar eureka-0.0.1-SNAPSHOT.jar
EXPOSE 8091:8091
ENTRYPOINT ["java","-jar","eureka-0.0.1-SNAPSHOT.jar"]